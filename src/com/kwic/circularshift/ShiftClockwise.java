package com.kwic.circularshift;

import java.util.LinkedList;
import java.util.List;

import com.kwic.bean.Line;
import com.kwic.bean.Word;


public class ShiftClockwise implements CircleShifter {

	private List<Line> lines;
	private int noOfShifts;
	
	@Override
	public List<Line> shift(Line line) {
		// TODO Auto-generated method stub
		lines = new LinkedList<Line>();
		lines.add(line);
		
		noOfShifts = line.getSize() - 1;
		
		for(int i = 0; i < noOfShifts; i++){
			Word word = line.deleteWord();
			line.addWord(word);
			Line lineCopy = new Line(line);
			lines.add(lineCopy);
		}
		
		return lines;
	}

}
