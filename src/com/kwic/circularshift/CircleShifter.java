package com.kwic.circularshift;

import java.util.LinkedList;
import java.util.List;

import com.kwic.bean.Line;


public interface CircleShifter {
	
	public List<Line> shift(Line line);

}
