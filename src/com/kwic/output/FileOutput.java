package com.kwic.output;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import com.kwic.utility.Utility;
import com.kwic.bean.Line;


public class FileOutput extends Output{
	
	private String fileName;
	private Path path;

	public void print(String fileName){
		try {
			Files.createFile(path);
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(path.getFileName().toString()));
			
			for(Line line : lines){
				capitalize(line);
				String lineStr = Utility.lineToString(line);
				fileWriter.write(lineStr);
				fileWriter.newLine();
			}
		fileWriter.close();	
		}
		
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setFileName(String name){
		this.fileName = name;
	}
	
	public void setPath(Path path){
		this.path = path;
	}

}
