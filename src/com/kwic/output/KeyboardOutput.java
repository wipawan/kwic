package com.kwic.output;

import com.kwic.bean.Line;
import com.kwic.utility.Utility;


public class KeyboardOutput extends Output{


	public void print(){
		System.out.println();
		for (Line line : lines){
			capitalize(line);
			String lineStr = Utility.lineToString(line);
			System.out.println(lineStr);
		}
		System.out.println();
	}
}

