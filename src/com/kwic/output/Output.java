package com.kwic.output;

import java.util.SortedSet;
import com.kwic.bean.Line;


public abstract class Output {

	//output is guranteed to receive a sorted set of lines
	protected SortedSet<Line> lines;
		
	public void setLines(SortedSet<Line> lines){
		this.lines = lines;
	}
	protected void capitalize(Line line){
			line.getLine().getFirst().capitalize();
	}
}
