package com.kwic.utility;

import java.util.LinkedList;
import java.util.StringTokenizer;

import com.kwic.bean.Line;
import com.kwic.bean.Word;

public class Utility {
	
	public static Line makeLine(String line){
		StringTokenizer tokenizer = new StringTokenizer(line, " ");
		Line lineObj = new Line();
		while (tokenizer.hasMoreTokens()){
			String token = tokenizer.nextToken();
			Word word = new Word(token);
			lineObj.addWord(word);
		}
		return lineObj;
	}
	
	public static String lineToString(Line line){
		String lineStr = "";
		LinkedList<Word> lineList = line.getLine();
		for(Word word : lineList){
			String wordStr = word.getWord();
			if (!lineList.iterator().hasNext()){
				lineStr = lineStr + wordStr;
			}
			else lineStr = lineStr + wordStr + " ";
		}
		return lineStr;
	}

}
