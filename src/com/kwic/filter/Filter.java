package com.kwic.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.kwic.bean.Line;
import com.kwic.bean.Word;


public class Filter {
	
	private List<Word> ignoreWordsList;
	protected String ignoreWordsString;
	//= {"is", "the", "of", "and", "as", "a", "after"};
	
	public Filter (String ignoreWords){
		ignoreWordsList = new ArrayList<Word>();
		StringTokenizer tokenizer = new StringTokenizer(ignoreWords, " ,");
		while (tokenizer.hasMoreTokens()){
			String ignoreWord = tokenizer.nextToken();
			ignoreWordsList.add(new Word(ignoreWord));
		}
//		for (int i =0; i < ignoreWordsString.length; i++){
//			Word ignoreWord = new Word(ignoreWordsString[i]);
//			ignoreWordsList.add(ignoreWord);
//		}
	}

	public List<Line> filter(List<Line> lines){
		
		List<Integer> indexToRemove = new ArrayList<Integer>();
		for (int i = 0; i < lines.size(); i++ ){
			Line line = lines.get(i);
			Word firstWord = line.getLine().getFirst();
			for (Word ignoreWord: ignoreWordsList){
				if (firstWord.getWord().compareTo(ignoreWord.getWord()) == 0){
					indexToRemove.add(i);
				}
			}//end of For(Word ignoreWord ..)
		}//end of for(Line line ..)
		
		//remove from back to front of lines list.
		for(int j = indexToRemove.size() - 1; j > -1; j-- ){
			int index = indexToRemove.get(j);
			lines.remove(index);
		}
		
		return lines;
	}
	
}//end of class
