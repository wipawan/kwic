package com.kwic.linestorage;

import java.util.Comparator;
import java.util.TreeSet;

import com.kwic.bean.Line;

public class LineStorage {

	private TreeSet<Line> inputLines;
	private TreeSet<Line> outputLines;

	public LineStorage(){
		inputLines = new TreeSet<Line>(new LineComparator());
		outputLines = new TreeSet<Line>(new LineComparator());
	}

	public TreeSet<Line> getInputLines(){
		return inputLines;
	}

	public TreeSet<Line> getOutputLines(){
		return outputLines;
	}

	public Line getFirstInputLine(){
		return inputLines.first();
	}

	public Line getFirstOutputLine(){
		return outputLines.first();
	}

	public void addInputLine(Line line){
		inputLines.add(line);
	}

	public void addOutputLine(Line line){
		outputLines.add(line);
	}


	private class LineComparator implements Comparator<Line>{
		@Override
		public int compare(Line line0, Line line1) {
			int result = line0.getLine().getFirst().getWord().compareTo(line1.getLine().getFirst().getWord());
			return result;
		}
	}

}
