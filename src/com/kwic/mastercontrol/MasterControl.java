package com.kwic.mastercontrol;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

import com.kwic.bean.Line;
import com.kwic.circularshift.ShiftClockwise;
import com.kwic.filter.Filter;
import com.kwic.input.FileInput;
import com.kwic.input.Input;
import com.kwic.input.KeyboardInput;
import com.kwic.linestorage.LineStorage;
import com.kwic.output.FileOutput;
import com.kwic.output.KeyboardOutput;

public class MasterControl {

	private static Path inputPath;
	private static boolean exitFlag = false;

	public static void main(String[] args){

		ShiftClockwise shiftClockwise = new ShiftClockwise();

		//start input
		Scanner in = new Scanner(System.in);
		
		consoleWrite("Welcome to the Key Word in Context program!\n");
		consoleWrite("Commands :-\n");
		consoleWrite("`stop` --- goes back to previous option.\n");
		consoleWrite("`exit` --- exits from the program.\n");
		
		
		while (!exitFlag){
			consoleWrite("\n");
			consoleWrite(">>>Key in `Words to Ignore` (eg.: a , after, the) : ");
			
			String ignoreWords = in.nextLine();
			Filter filter =  new Filter(ignoreWords);
			
			consoleWrite(">>>Input Method: Keyboard or File? : ");
			
			LineStorage lineStorage = new LineStorage();
			
			switch(in.nextLine().toLowerCase()){
			case "keyboard" :
				KeyboardInput keyboardInput = new KeyboardInput();
				consoleWrite(">>>Enter filename (input.txt) or path (d:/input.txt) to save your inputs in. : ");
				
				String pathKeyboard = in.nextLine().toLowerCase();
				
				consoleWrite("\n");
				
				if (!pathKeyboard.equals("stop")){
					inputPath = Paths.get(pathKeyboard); //set path for file
					keyboardInput.setPath(inputPath);
					//in.close(); //close in for keyboard input to open up its own scanner.in

					batchProcess(keyboardInput,lineStorage, shiftClockwise, filter);

					KeyboardOutput keyboardOutput = new KeyboardOutput();
					keyboardOutput.setLines(lineStorage.getOutputLines());
					keyboardOutput.print();
				}
				else break;

				break;

			case "file" :
				FileInput fileInput = new FileInput();
				consoleWrite(">>>Enter Filename (input.txt) or Path (d:/input.txt) of input file: ");

				while (in.hasNextLine()){
					
					String pathFile = in.nextLine().toLowerCase();
					
					if (!pathFile.equals("stop")){

						//Set Path for Input
						inputPath = Paths.get(pathFile);
						fileInput.setPath(inputPath);
						consoleWrite("\n");
						consoleWrite("Path set: " + inputPath.toString() + "\n");
						
						batchProcess(fileInput,lineStorage, shiftClockwise, filter);
						
						consoleWrite("Circular Shift complete!\n");
						consoleWrite("\n");
						
						//send to fileoutput to save
						consoleWrite(">>>Enter filename/path to save results: ");
						String savePathStr = in.nextLine();

						FileOutput fileOutput = new FileOutput();
						fileOutput.setPath(Paths.get(savePathStr));
						fileOutput.setLines(lineStorage.getOutputLines());
						fileOutput.print(savePathStr);

						consoleWrite("File saved in " + savePathStr + "\n");
						consoleWrite("\n");
						consoleWrite(">>>Enter Filename (input.txt) or Path (d:/input.txt) of input file: ");

					}
					else break;
				}
				break;

			case "exit" :
				exitFlag = true;
			
			default:
				consoleWrite("Commands :-\n");
				consoleWrite("`stop` --- goes back to previous option.\n");
				consoleWrite("`exit` --- exits from the program.\n");
			}//end of switch
		}
	} 
	private static void consoleWrite(String string){
		System.out.print(string);
	}

	private static void batchProcess(Input input, LineStorage lineStorage, ShiftClockwise shiftClockwise, Filter filter){

		//MC Store Lines
		List<Line> inputLines = input.getLines();

		for(Line line : inputLines){
			lineStorage.addInputLine(line);
		}

		//MC Call Shift for each input line
		for(Line line : inputLines){
			List<Line> shiftedLines = shiftClockwise.shift(line);

			//filter shifted lines
			List<Line> filteredLines = filter.filter(shiftedLines);

			//MC Call LineStorage store filtedLines into outputLines
			for(Line filteredLine : filteredLines){
				lineStorage.addOutputLine(filteredLine);
			}
		}
	}

}
