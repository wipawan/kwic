package com.kwic.bean;

import java.util.LinkedList;

public class Line {
	
	private LinkedList<Word> line;
	
	public Line(){
		line = new LinkedList<Word>();
	}
	
	public Line(Line line){
		this.line = new LinkedList<Word>();
		for(Word word : line.getLine()){
			this.line.addLast(word);
		}
	}
	
	public void addWord(Word word){
		line.addLast(word);
	}
	
	public Word deleteWord(){
		return line.removeFirst();
	}
	
	public LinkedList<Word> getLine(){
		return line;
	}
	
	public int getSize(){
		return line.size();
	}
	
}
