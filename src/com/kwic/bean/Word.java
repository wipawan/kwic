package com.kwic.bean;

public class Word implements Comparable<Word> {

	private String word;
	
	public Word(){
		word = "";
	}
	
	public Word(String newWord){
		this.word = newWord;
	}
	
	public String getWord(){
		return word;
	}
	
	public void capitalize(){
		if(!word.equals("")){
			char[] stringArray = word.toCharArray();
			stringArray[0] = Character.toUpperCase(stringArray[0]);
			word = new String(stringArray);
		}
	}
	
	public int compareTo(Word anotherWord){
		
		boolean compareResult = word.equalsIgnoreCase(anotherWord.getWord());
		
		return compareResult? 1 : 0;
	}
}
