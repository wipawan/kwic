package com.kwic.input;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import com.kwic.bean.Line;
import com.kwic.bean.Word;


public class FileInput extends Input {

	private String fileName;
	private Path path;
	
	public List<Line> getLines() {
		
		List<Line> lineList = new ArrayList<Line>();
		
		try {
			Scanner fileReader = new Scanner(new FileReader(path.getFileName().toString()));
			while (fileReader.hasNextLine()){
				String lineStr = fileReader.nextLine();
				Line lineObj = makeLine(lineStr);
				lineList.add(lineObj);
			}
			fileReader.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
		return lineList;
	}
	
	public void setFileName(String name){
		this.fileName = name;
	}
	
	public void setPath(Path path){
		this.path = path;
	}
	
	private Line makeLine(String line){
		StringTokenizer tokenizer = new StringTokenizer(line, " ");
		Line lineObj = new Line();
		while (tokenizer.hasMoreTokens()){
			String token = tokenizer.nextToken();
			Word word = new Word(token);
			lineObj.addWord(word);
		}
		return lineObj;
	}

}
