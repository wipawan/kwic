package com.kwic.input;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.kwic.bean.Line;
import com.kwic.utility.Utility;


public class KeyboardInput extends Input {

	public List<Line> getLines() {

		Scanner in = new Scanner(System.in);
		
		List<Line> inputList = new ArrayList<Line>();

		try {
			Files.createFile(path); //create file
			int lineNo = 1;

			try {
				BufferedWriter fileWriter = new BufferedWriter(new FileWriter(path.getFileName().toString()));

				System.out.print("Enter your lines, each ending with the return key.\n" +
						"Enter an empty line to end input sequence.\n" +
						">>>Line " + lineNo + ".: ");
				
				String line = in.nextLine();

				while (!line.equals("")){

					//create the line and temp store it
					Line lineObj = Utility.makeLine(line);
					inputList.add(lineObj);

					//save the line into a file (non-functional)
					try {
						fileWriter.write(line);
						fileWriter.newLine();
					}
					catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					lineNo++;
					System.out.print(">>>Line " + lineNo + ".: ");
					line = in.nextLine();
				}//end of while

				fileWriter.close();
			}//end of try

			catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
			catch (FileAlreadyExistsException e) {
				System.err.println("already exists: " + e.getMessage());
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			//in.close();
			return inputList;
		}

	public void setPath(Path path){
		this.path = path;
	}
}
