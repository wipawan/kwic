package com.kwic.input;

import java.nio.file.Path;
import java.util.List;

import com.kwic.bean.Line;


public abstract class Input {

	private List<Line> lines;
	protected Path path;
	
	public abstract List<Line> getLines();
	
	public abstract void setPath(Path path);
}
